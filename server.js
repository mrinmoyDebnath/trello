import express from "express";
import { fileURLToPath } from 'url';
import path from 'path';

const port = process.env.PORT || 3000;
const app = express();
const __dirname = path.dirname(fileURLToPath(import.meta.url));
app.use('/static', express.static(path.resolve(__dirname, 'frontend', 'static')))

app.get('/*', (req, res)=>{
    res.sendFile(path.resolve(__dirname, 'frontend', 'index.html'))
});

app.listen(port, ()=>console.log(`server running`))