import createCheckItem from '../api/createCheckItem.js';
import deleteChecklist from '../api/deleteChecklist.js';
import getChecklist from '../api/getChecklist.js';

export default async function Checklists(checklistId){
    const checklist = await getChecklist(checklistId);
    const checkListContainer = document.createElement('div');
    checkListContainer.innerHTML = `
        <div class="font-bold text-xl mt-4"><h2>${checklist.name}</h2></div>
        <div class="flex justify-between">
            <div class="py-2  px-4 bg-gray-300 rounded shadow-sm hover:bg-gray-400 my-2 cursor-pointer">Add item</div>
            <div><i class="fas fa-trash-alt delete-btn cursor-pointer" style="margin:auto;"></i></div>
        </div>
    `
    const createitemBtn = checkListContainer.lastElementChild.firstElementChild;
    createitemBtn.addEventListener('click', e=>addCheckItem(checklistId, checkListContainer));
    const deleteChecklistBtn = checkListContainer.lastElementChild.lastElementChild;
    deleteChecklistBtn.addEventListener('click', e=>removeChecklist(checklistId))
    const checkItemsList = checklist.checkItems;
    const checkItems = document.createElement('ul');
    checkItems.id = `${checklistId}`;
    checkItems.innerHTML = ``
    for(let i=0; i<checkItemsList.length; i++){
        const checkItem = checkItemsList[i];
        checkItems.innerHTML += `
            <li id="${checkItem.id}" class="m-2">
                <input type="checkbox">&nbsp;&nbsp;&nbsp;${checkItem.name}</input>
            </li>
        `
    }
    const checkListElement = document.createElement('div');
    checkListElement.appendChild(checkItems);
    checkListContainer.appendChild(checkListElement);
    return checkListContainer;
}

async function addCheckItem(checklistId, checkListContainer){
    const modalArea = document.querySelector('#modal-area');
    const newArea = document.createElement('div');
    newArea.className = 'modal';
    modalArea.appendChild(newArea);
    newArea.classList.remove('hidden');
    newArea.innerHTML = `
    <div id="checkItem" class="modal-item p-2 rounded">
        <form>
            <div id="input-checkItem" class="input-area">
                <input type="text" placeholder="Add checkitem name">
            </div>
            <div id="submit-checkItem-btn" class="form-btn float-left p-2 mt-3 rounded shadow cursor-pointer">
                <input type="submit" value="Submit">
            </div>
            <div id="cancel-checkItem-btn" class="form-btn float-right p-2 mt-3 rounded shadow cursor-pointer">Cancel</div>
        </form>
    </div>`
    newArea.firstElementChild.firstElementChild.lastElementChild.addEventListener('click', e => {
        e.preventDefault();
        newArea.classList.add('hidden');
    })
    newArea.addEventListener('submit', async e => {
        console.log(e.target)
        e.preventDefault();
        const checkitemName = e.target.elements[0].value;
        // // // console.log(checkitemName==='')
        if (checkitemName !== '') {
            const item = await createCheckItem(checklistId, checkitemName);
            console.log(item)
            checkListContainer.lastElementChild.lastElementChild.innerHTML += `
                <li id="${item.id}" class="m-2">
                    <input type="checkbox">&nbsp;&nbsp;&nbsp;${item.name}</input>
                </li>
            `
            
        }
        newArea.classList.add('hidden');
    })  
}

async function removeChecklist(checklistId){
    const modalArea = document.querySelector('#modal-area');
    const newArea = document.createElement('div');
    newArea.className = 'modal';
    modalArea.appendChild(newArea);
    newArea.classList.remove('hidden');
    newArea.innerHTML = `
    <div id="checkItem" class="modal-item p-2 rounded">
        <form>
            <div id="input-checkItem" class="input-area">
                <input type="text" placeholder="Type delete to confirm">
            </div>
            <div id="submit-checkItem-btn" class="form-btn float-left p-2 mt-3 rounded shadow cursor-pointer">
                <input type="submit" value="Confirm">
            </div>
            <div id="cancel-checkItem-btn" class="form-btn float-right p-2 mt-3 rounded shadow cursor-pointer">Cancel</div>
        </form>
    </div>`
    newArea.firstElementChild.firstElementChild.lastElementChild.addEventListener('click', e => {
        e.preventDefault();
        newArea.classList.add('hidden');
    })
    newArea.addEventListener('submit', async e => {
        e.preventDefault();
        const confirmDelete = e.target.elements[0].value;
        // // // console.log(confirmDelete==='')
        if (confirmDelete === 'delete') {
            const item = await deleteChecklist(checklistId);
            location.reload();
        }
        newArea.classList.add('hidden');
    })  
}