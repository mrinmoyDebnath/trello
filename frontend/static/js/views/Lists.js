import getLists from '../api/getLists.js';
import CardsView from "./Cards.js";
import boardName from '../api/getBoardDetail.js';
import createList from '../api/createList.js';
import deleteList from '../api/deleteList.js';

export default async function ListsView(boardId) {
    const listPage = document.createElement('ul');
    // listPage.innerHTML = '';
    const board = await boardName(boardId);
    // console.log(board);
    const boardView =document.createElement('div');
    boardView.innerHTML = `<h1 style="text-align: center;" class="font-bold text-3xl mt-4" >${board.name}</h1 style="text-align: center;">`
    listPage.className = 'font-bold whitespace-nowrap overflow-hidden flex justify-start';
    const lists = await getLists(boardId);
    // console.log(lists);
    for(let i=0; i< lists.length; i++){
        const listItem = document.createElement('li');
        listItem.className = 'm-4 list-item rounded p-4 font-bold'
        listItem.innerHTML = `
            <div class='flex justify-between'>
                <div><h4>${lists[i].name}</h4></div>
                <div><i class="fas fa-trash-alt delete-btn cursor-pointer" style="margin: auto;"></i></div>
            </div>
        `;
        const deleteListBtn = listItem.firstElementChild.lastElementChild;
        deleteListBtn.addEventListener('click', e=>removeList(lists[i].id));
        const cardElement = await CardsView(lists[i].id);

        listItem.appendChild(cardElement);

        listPage.appendChild(listItem);
    }
    const addNewList = document.createElement("li");
    addNewList.className = 'add-new-list m-4 list-item rounded p-4 cursor-pointer';
    addNewList.innerText = '+ Add another list';
    addNewList.addEventListener('click', e=>addList(boardId))
    listPage.appendChild(addNewList)
    listPage.addEventListener('wheel', e=>{
        if (e.deltaY > 0) listPage.scrollLeft += 100;
          else listPage.scrollLeft -= 100;
    })
    boardView.appendChild(listPage);
    return boardView;
}


function addList(boardId) {
    const modalArea = document.querySelector('#modal-area');
    modalArea.classList.remove('hidden');
    while (modalArea.firstChild) {
        modalArea.removeChild(modalArea.lastChild);
    }
    modalArea.innerHTML = `
        <div id="create-list" class="modal-item p-2 rounded">
            <form>
                <div id="input-list" class="input-area">
                    <input type="text" placeholder="Add list title">
                </div>
                <div id="create-list-btn" class="form-btn float-left p-2 mt-3 rounded shadow cursor-pointer">
                    <input type="submit" value="submit">
                </div>
                <div id="cancel-list-btn" class="form-btn float-right p-2 mt-3 rounded shadow cursor-pointer">Cancel</div>
            </form>
        </div>`
        modalArea.firstElementChild.firstElementChild.lastElementChild.addEventListener('click', e=>{
            e.preventDefault();
            // console.log(e.target);
            modalArea.classList.add('hidden');
        })
        modalArea.firstElementChild.firstElementChild.addEventListener('submit', async e=>{
            e.preventDefault();
            const listName = e.target.elements[0].value;
            // console.log(listName==='')
            if(listName!==''){
                await createList(boardId, listName);
                location.reload();
            }
            modalArea.classList.add('hidden');
        })
}

async function removeList(listId){
    const modalArea = document.querySelector('#modal-area');
    modalArea.classList.remove('hidden');
    while (modalArea.firstChild) {
        modalArea.removeChild(modalArea.lastChild);
    }
    modalArea.innerHTML = `
        <div class="modal-item p-2 rounded">
            <form>
                <div class="input-area">
                    <input type="text" placeholder="Type delete to confirm">
                </div>
                <div class="form-btn float-left p-2 mt-3 rounded shadow cursor-pointer">
                    <button type="submit" >Confirm</button>
                </div>
                <div class="form-btn float-right p-2 mt-3 rounded cursor-pointer shadow">Cancel</div>
            </form>
        </div>`
    modalArea.firstElementChild.firstElementChild.lastElementChild.addEventListener('click', e=>{
        e.preventDefault();
        // console.log(e.target);
        modalArea.classList.add('hidden');
    })
    modalArea.firstElementChild.firstElementChild.addEventListener('submit', async e=>{
        e.preventDefault();
        const confirmDelete = e.target.elements[0].value;
        // console.log(confirmDelete==='')
        if(confirmDelete === 'delete'){
            const item = await deleteList(listId);
            console.log(item)
            location.reload();
        }
        modalArea.classList.add('hidden');
    })
}