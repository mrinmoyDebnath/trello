import getBoards from '../api/getBoards.js'
import createBoard from '../api/createBoard.js';
import deleteBoard from '../api/deleteBoard.js';

export default async function BoardView() {
    const boardContainer = document.createElement('div');
    boardContainer.id = 'board-container';
    const boardList = document.createElement('ul');
    const boards = await getBoards();
    for(let i=0; i<boards.length; i++){
        const boardItem = document.createElement('li');
        boardItem.className = `m-4 board-li float-left`
        boardItem.innerHTML = `
            <a href="/board/id:${boards[i].id}" data-link>
                <div class='board-item pl-4 pt-2 font-bold'>${boards[i].name}</div>
            </a>
            <div class='delete-board'><i class="fas fa-trash-alt float-right mr-1 mb-1 delete-btn cursor-pointer"></i></div>
        `
        const deleteBtn = boardItem.lastElementChild.firstElementChild;
        deleteBtn.addEventListener('click', e=>removeBoard(boards[i].id));
        boardList.appendChild(boardItem);
    };
    // boardList.innerHTML = boardItems;
    boardList.className = 'list-none p-8'
    const addNewBoard = document.createElement('li');
    addNewBoard.innerHTML = `<div class='w-full h-full cursor-pointer'>Create new board</div>`
    addNewBoard.className = 'm-4 add-board float-left';
    addNewBoard.addEventListener('click', addBoard);
    boardList.appendChild(addNewBoard);
    boardContainer.appendChild(boardList);
    return boardContainer;
}

function addBoard(){
    const modalArea = document.querySelector('#modal-area');
    modalArea.classList.remove('hidden');
    while (modalArea.firstChild) {
        modalArea.removeChild(modalArea.lastChild);
    }
    modalArea.innerHTML = `
        <div id="create-board" class="modal-item p-2 rounded">
            <form>
                <div id="input-board" class="input-area">
                    <input type="text" placeholder="Add board title">
                </div>
                <div id="create-board-btn" class="form-btn float-left p-2 mt-3 rounded shadow cursor-pointer">
                    <button type="submit" >Submit</button>
                </div>
                <div id="cancel-board-btn" class="form-btn float-right p-2 mt-3 rounded cursor-pointer shadow">Cancel</div>
            </form>
        </div>`
    modalArea.firstElementChild.firstElementChild.lastElementChild.addEventListener('click', e=>{
        e.preventDefault();
        // console.log(e.target);
        modalArea.classList.add('hidden');
    })
    modalArea.firstElementChild.firstElementChild.addEventListener('submit', async e=>{
        e.preventDefault();
        const boardName = e.target.elements[0].value;
        // console.log(boardName==='')
        if(boardName!==''){
            await createBoard(boardName);
            location.reload();
        }
        modalArea.classList.add('hidden');
    })
}

async function removeBoard(boardId){
    const modalArea = document.querySelector('#modal-area');
    modalArea.classList.remove('hidden');
    while (modalArea.firstChild) {
        modalArea.removeChild(modalArea.lastChild);
    }
    modalArea.innerHTML = `
        <div class="modal-item p-2 rounded">
            <form>
                <div class="input-area">
                    <input type="text" placeholder="Type delete to confirm">
                </div>
                <div class="form-btn float-left p-2 mt-3 rounded shadow cursor-pointer">
                    <button type="submit" >Confirm</button>
                </div>
                <div class="form-btn float-right p-2 mt-3 rounded cursor-pointer shadow">Cancel</div>
            </form>
        </div>`
    modalArea.firstElementChild.firstElementChild.lastElementChild.addEventListener('click', e=>{
        e.preventDefault();
        // console.log(e.target);
        modalArea.classList.add('hidden');
    })
    modalArea.firstElementChild.firstElementChild.addEventListener('submit', async e=>{
        e.preventDefault();
        const confirmDelete = e.target.elements[0].value;
        // console.log(confirmDelete==='')
        if(confirmDelete === 'delete'){
            const item = await deleteBoard(boardId);
            console.log(item)
            location.reload();
        }
        modalArea.classList.add('hidden');
    })
}