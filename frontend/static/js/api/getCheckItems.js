export default async function getCheckItem(checkItemId, checklistId) {
    try {
        const response = await fetch(`https://api.trello.com/1/checklists/${checklistId}/checkItems/${checkItemId}?key=ffbf49859c4893fd9173220097e64330&token=f99f5b483f86659f167ddb52a4621e7608b7122ac1ecb9da02cafe4b94cb541f`, {
            method: 'GET'
        })
        // console.log(response);
        return response.json()
    } catch (err) {
        console.error(err);
    }
}