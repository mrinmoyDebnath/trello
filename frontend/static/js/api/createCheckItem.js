export default async function createCheckItem(checklistId, name) {
    try {
        const response = fetch(`https://api.trello.com/1/checklists/${checklistId}/checkItems?key=ffbf49859c4893fd9173220097e64330&token=f99f5b483f86659f167ddb52a4621e7608b7122ac1ecb9da02cafe4b94cb541f&name=${name}`, {
            method: 'POST'
        })
        return (await response).json()
    } catch (err) {
        console.error(err);
    }
}