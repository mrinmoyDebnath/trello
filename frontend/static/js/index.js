import Boards from './views/Boards.js'
import Lists from './views/Lists.js'

const navigateTo = url =>{
    history.pushState(null, null, url);
    router();
};

const router = async () =>{
    const routes = [
        {path: '/', view: Boards},
        {path: '/board', view: Lists},
    ];
    let id, name;
    const path = location.pathname.split('/');
    if(path.length===3){
        id = path[2].split(':')[1];
    }else if(path.length===4){
        id = path[2].split(':')[1];
        name = path[3].split(':')[1];
    }
    // console.log(location.pathname)
    const potentialMatches = routes.map(route=>{
        return {
            route,
            isMatch: location.pathname.split('/')[1] === route.path.split('/')[1],
            id,
            name
        }
    })
    let match = potentialMatches.find(potentialMatch => potentialMatch.isMatch)
    if(!match){
        match = {
            route: routes[0],
            isMatch: true,
            id,
            name,
        }
    }
    // console.log('match: ', match)

    const rootNode = document.getElementById('root');
    let view;
    if(match.name){
        view = await match.route.view(match.id, match.name);
    } else if(match.id){
        view = await match.route.view(match.id);
    } else{
        view = await match.route.view();
    }
    while (rootNode.firstChild) {
        rootNode.removeChild(rootNode.lastChild);
    }
    rootNode.appendChild(view)
};

window.addEventListener('popstate', router);

document.addEventListener('DOMContentLoaded', ()=>{
    document.body.addEventListener('click', e=>{
        if(e.target.matches('[data-link]')){
            e.preventDefault();
            // console.log(e.target.href)
            navigateTo(e.target.href);
        }
    })
    router()
});